# KACtUS

![kactus](design/logo_small.png)

**K**eep **A**ir **C**lean **t**o **US**

Part of the [Aireamos.org project](https://aireamos.org), member of the [Covid Warriors Community](https://www.covidwarriors.org).

## MEMBERS
* mgesteiro
* heba.es

## LICENSE

This work is licensed under the [GNU General Public License v3.0](LICENSE-GPLV30). All media and data files that are not source code are licensed under the [Creative Commons Attribution 4.0 BY-SA license](LICENSE-CCBYSA40).

More information about licenses in [Opensource licenses](https://opensource.org/licenses/) and [Creative Commons licenses](https://creativecommons.org/licenses/).
